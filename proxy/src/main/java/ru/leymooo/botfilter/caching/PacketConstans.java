package ru.leymooo.botfilter.caching;

/**
 *
 * @author Leymooo
 */
public class PacketConstans
{

    private PacketConstans()
    {
    }
    
    public static final int LOGIN = 0;
    public static final int CHUNK = 1;
    public static final int TIME = 2;
    public static final int PLAYERABILITIES = 3;
    public static final int PLAYERPOSANDLOOK_CAPTCHA = 4;
    public static final int SETSLOT_MAP = 5;
    public static final int SETSLOT_RESET = 6;
    public static final int KEEPALIVE = 7;
    public static final int CAPTCHA_FAILED_2 = 8;
    public static final int CAPTCHA_FAILED_1 = 9;
    public static final int CHECKING = 10;
    public static final int CHECKING_CAPTCHA = 11;
    public static final int CHECK_SUS = 12;
    public static final int PLAYERPOSANDLOOK = 13;
    public static final int SETEXP_RESET = 14;
    
    

}

BungeeCord with built in AntiBot protection. (Russian lang)
==========

Videos
--------
Captcha+Falling check:
[![Only captcha](https://i.ytimg.com/vi/rbZ3Xa-C3pM/2.jpg)](https://youtu.be/rbZ3Xa-C3pM)
Falling check:
[![Only captcha](https://i.ytimg.com/vi/P1MA7ZmR2Ik/1.jpg)](https://youtu.be/P1MA7ZmR2Ik)

Download
--------
You can download this protection on [RuBukkit](http://www.rubukkit.org/threads/137038/) or on [Yandex Disk](https://yadi.sk/d/QrSeJWeU3LAq4p)
